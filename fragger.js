/** @module fragger
 */

/** A class to manage round trips between fragment strings & objects.
 *
 *  @class
 *  @name Fragger
 *  @classdesc
 *      Makes an object which holds config for round tripping data between
 *      fragment strings and JS objects, and offers decode(fragment) and
 *      encode(obj) methods which share that config. Call this with a config to
 *      make an object, keep the object around and call its encode and decode
 *      methods as you need to.
 *
 *  @param {object} schema
 *      @param {function} schema.encode
 *          serializer function
 *      @param {function} schema.decode
 *          deserializer function
 *      @param {function} schema.validate
 *          function to check object is okay to use
 *      @param {number} schema.max_length
 *          Values of greater length than this will be rejected.
 *
 *  @param {object} options
 *      @param {string} options.sep
 *          Character used to delimit fields.
 *      @param {string} options.eq
 *          Character used to set apart a field's key from its value.
 *        @param {*} options.implicit_value
 *          The value to omit when serializing and assume when deserializing
 *      @param {number} options.max_length
 *          Fragments larger than this are not processed.
 *
 *  @returns {Fragger} 
 *      An object exposing encode and decode methods.
 */
function Fragger(schema, options) {
    "use strict";

    if (!(this instanceof Fragger)) {
        return new Fragger(schema, options);
    }

    // Marshal options (without dragging in defaulter this time)
    var original_options = options || {};
    var defaults = {
        sep: '&',
        eq: ':',
        implicit_value: undefined,
        max_length: 2048,
        debug: false
    };
    options = {};
    var property;
    for (property in defaults) {
        if (defaults.hasOwnProperty(property)) {
            if (original_options[property] === undefined) {
                options[property] = defaults[property];
            }
            else {
                options[property] = original_options[property];
            }
        }
    }

    var problem;
    problem = this.check_options(options);
    if (problem) {
        throw new Error(problem);
    }
    problem = this.check_schema(schema);
    if (problem) {
        throw new Error(problem);
    }

    /** Converts a fragment string to an object.
     *
     *  @method Fragger#decode
     *
     *  @param {string} fragment
     *      Fragment string to convert (deserialize to object).
     *
     *  @returns {object}
     *  Normally the object should contain the properties specified in the
     *  schema.
     *  If you pass a string with no data, this may return an empty object.
     *
     *  @throws {Error}
     *  If you pass something nutty like null or undefined or a non-string or
     *  a string larger than max_length, this will throw an Error.
     **/
    var decode = function(fragment) {
        if (fragment === null || fragment === undefined) {
            throw new Error("nothing to deserialize");
        }
        if (typeof fragment != 'string') {
            throw new Error("can't deserialize non-string");
        }
        var obj = _fragment_to_object(fragment, schema, options);
        return obj;
    };

    /** Convert an object's own properties to a fragment string.
     *
     *  @method Fragger#encode
     *
     *  @param {object} obj
     *      Object to convert (serialize to string).
     *
     *  @returns {string}
     *  A string serializing own properties from the given object, as specified
     *  in the schema. 
     *
     *  If you pass an empty object, this may return an empty string.
     *
     *  If the schema failed on one or more properties of the object, this
     *  returns null to avoid giving you bad data or throwing an error.
     *  It is up to you to check for null and deal with schema failures in
     *  a way that is appropriate for your app.
     *
     *  @throws {Error}
     *  If you pass something nutty like null or undefined or a string, this
     *  will throw an Error. 
     **/
    var encode = function(obj) {
        if (obj === null || obj === undefined) {
            throw new Error("nothing to serialize");
        }
        if (typeof obj == 'string') {
            throw new Error("can't serialize string as if it were object");
        }
        var fragment = _object_to_fragment(obj, schema, options);
        return fragment;
    };

    return {
        decode: decode,
        encode: encode
    };
}

/** @method */
Fragger.prototype.check_schema = function(schema) {
    "use strict";

    if (!schema) {
        return 'schema is not an object';
    }

    var property_count = 0;
    var no_value = [],
        no_encode = [],
        no_validate = [];
    var value;

    for (var property in schema) {
        if (schema.hasOwnProperty(property)) {
            property_count += 1;
            value = schema[property];
            if (!value) {
                no_value.push(property);
            }
            if (!value.encode) {
                no_encode.push(property);
            }
            if (!value.validate) {
                no_validate.push(property);
            }
        }
    }
    if (property_count === 0) {
        return 'schema is empty';
    }
    var defects = [];
    if (no_value.length > 0) {
        defects.push("no values on " + no_value.toString());
    }
    if (no_encode.length > 0) {
        defects.push("no encode for " + no_encode.toString());
    }
    if (no_validate.length > 0) {
        defects.push("no validate for " + no_validate.toString());
    }
    if (defects.length > 0) {
        return ("defective schema: " + defects.join('; '));
    }
    return null;
};

/** @method */
Fragger.prototype.check_options = function(options) {
    if (options.sep === '') {
        return ("empty sep would make unparseable fragments");
    }
    if (options.eq === '') {
        return ("empty eq is likely to make unparseable fragments");
    }
    if (options.max_length < 1) {
        return ("max_length < 1 won't let Fragger do anything");
    }
    return null;
};

/** Decode an object from a URL fragment string.
 *
 *  @private
 *
 *  @param {string} fragment
 *      URL fragment string to deserialize.
 *  @param {object} schema
 *      Object with certain properties.
 *  @param {object} options
 *      @param {string} sep
 *          character used to delimit fields.
 *      @param {string} eq
 *          Character used to set apart a field's key from its value.
 *      @param {number} max_length
 *          Fragments larger than this are not processed.
 */
function _fragment_to_object(fragment, schema, options) {
    "use strict";
    // We don't process defaults here. options has to contain the right things.
    // if you want defaulting, use Fragger
    options = options || {debug: false};
    if (options.debug) {
        console.log("in _fragment_to_object", fragment, schema, options);
    }
    // Decline to handle degenerate values.
    // Caller should NOT be passing these.
    if (!fragment || fragment === '#') {
        throw new Error("no fragment given: " + fragment);
    }
    if (fragment.length > options.max_length) {
        throw new Error(
            "fragment length " + fragment.length.toString() +
            " > max_length " + options.max_length.toString()
        );
    }
    if (fragment[0] == '#') {
        fragment = fragment.slice(1);
        // throw new Error("crazy fragment: " + fragment);
    }
    // Iterate through split-up text.
    var chunks = fragment.split(options.sep);
    var result = {};
    var pair;
    var entry;
    var parsed;
    for (var i=0; i<chunks.length; i++) {
        pair = chunks[i].split(options.eq);
        if (!schema.hasOwnProperty(pair[0])) {
            continue;
        }
        parsed = null;
        if (pair.length == 1) {
            if (options.debug) { 
                console.log("implicit value", pair, options.implicit_value); 
            }
            parsed = options.implicit_value;
        }
        else {
            entry = schema[pair[0]]; 
            if (entry && entry.decode) {
                parsed = entry.decode(pair[1]);
            }
            else {
                parsed = pair[1];
            }
            if (options.debug) {
                console.log("parsed", pair, entry, parsed); 
            }
        }
        result[pair[0]] = parsed;
    }
    return result;
}

/** Encode a dict-like object into a certain kind of URL fragment string.
 *
 *  In the simplest case, we have one key/value mapping at the top level,
 *  with each key/value chunk separated by sep (e.g. '&')
 *  and each key separated from its value by eq (e.g. '=').
 *  (This won't do straight up 'http://foo#bar,baz'. There's always at least
 *  one key/value pair.)
 *
 *  keys are always strings.
 *  But in fancy cases, a value might be something other than a string.
 *  A value which is a list of strings can be represented as a sequence of keys
 *  using sep but no values, like "foo&bar".
 *  A value which is an object mapping to strings can be represented the same
 *  way the top-level object is, but a different set of delimiters.
 *
 *  @private
 *
 *  @param {object} obj
 *      the Javascript object to serialize.
 *      only its own properties will be included.
 *  @param {object} schema
 *      Used to check whether an object property can be put in the fragment.
 *      keys are names of whitelisted properties on the passed object.
 *      values are function(value) {} returning true for acceptable values.
 *      If schema is not defined, no schema checks are done.
 *      But if there is a schema, a property must be listed in it to be encoded
 *      in the fragment.
 *  @param {string} sep
 *      character used to delimit fields.
 *  @param {string} eq
 *      Character used to set apart a field's key from its value.
 */

function _object_to_fragment(obj, schema, options) {
    "use strict";
    // No defaulting here, options has to contain the right things.
    // Without this constraint, we don't have anything parseable.
    // keys with these values are omitted.
    options = options || {debug: false};
    var omitted_values = [undefined, null, false];
    var chunks = [];
    var value;
    if (!obj || !schema) {
        return null;
    }
    var entry;
    if (options.debug) { 
        console.log("object_to_fragment"); 
    }
    var schema_property_count = 0;
    for (var key in obj) {
        if (!schema.hasOwnProperty(key)) {
            continue;
        }
        schema_property_count += 1;
        entry = schema[key];
        value = obj[key];
        if (!entry || !entry.validate || !entry.encode || !entry.validate(value)) {
            if (options.debug) {
                console.log("schema failed on key", key, value, entry); 
            }
            // throw new Error("schema failed for key " + key);
            return null;
        }
        // Omit keys with omitted values
        var should_omit = false;
        var i = omitted_values.length;
        while (i--) {
            if (omitted_values[i] == value) {
                should_omit = true;
                break;
            }
        }
        if (should_omit) {
            if (options.debug) { 
                console.log("omitting", key, value); 
            }
            continue;
        } else {
            if (options.debug) { 
                console.log("not omitting", key, value); 
            }
        }
        if (options.implicit_value == value) {
            if (options.debug) { 
                console.log("inferring implicit value for key", key); 
            }
            chunks.push(key);
            continue;
        }
        var encoded = entry.encode(value);
        if (encoded) {
            chunks.push([key, encoded].join(options.eq));
        }
    }
    if (schema_property_count === 0) {
        return null;
    }
    chunks.sort();
    if (chunks.length === 0) {
        if (options.debug) {
            console.log("no chunks to join");
        }
        return "";
    }
    return chunks.join(options.sep);
}

// expose private for testing
Fragger._object_to_fragment = _object_to_fragment;
Fragger._fragment_to_object = _fragment_to_object;

module.exports = {
    /** Fragger is a function called with schema and options objects
     * defining a configuration. It returns an object you can use to encode
     * objects as fragment strings, and decode fragment strings as objects.
     **/
    Fragger: Fragger
};
