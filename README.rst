Fragger
=======

This is just a Javascript helper library for putting data in the URL fragment
and getting it back again. If you like reading code, reading the code will be
much more precise than this narrative doc.

Here's the intended workflow:

* Make a Fragger object with your desired configuration. 
  The key part of this is to specify a schema which says which properties you
  want to deal with, how to encode them to strings, and how to validate them,
  and optionally how to decode them to objects.
* You can freely call fragger.encode(obj), where obj is a hash/dict style
  object (own properties), to get back a string suitable for putting in the
  fragment.
* You can freely call fragger.decode(fragment), where fragment is a string, to
  get back a Javascript object.


Installing
----------

If you use `component <http://component.io>`_ to manage your frontend
dependencies for a project, you can add this to the project simply:

    $ component install sashahart/fragger

If you just want to use the Javascript file from the repo, that's OK too. But
it's packaged as a CommonJS module. So find a way of using CommonJS modules, or
extract the bits you need. 


Design goals
-------------

Fragger's design is guided by two basic ideas.

* Do just one thing, leaving other tasks open.

This is intentionally just a standalone utility. It doesn't interact with the
DOM at all, doesn't mess with history, doesn't listen to hashchange events. It
doesn't provide fancy serialization/deserialization. It doesn't specifically
integrate with jQuery or Underscore or whatever. These kinds of things are up
to you. You also provide the logic for encode, decode, and validate. 

All this is just to keep things simple and a la carte, therefore flexible.

* Be strict on bad input.

On the object->fragment side, the concern is not to leak data into the fragment
that you did not want to put there. So it won't extract any property from an
object which isn't an own property. Or which has a value that doesn't validate.
Or which you didn't give a validate function for, in other words which you
didn't specifically insist on publishing.

On the fragment->object side, the concern is to prevent bad input from making
fragger do something lame, like choking or propagating wrong data to its
callers. So it won't deal with overly long strings. It won't parse properties
that aren't in the schema. If you provided a decode function for a property,
that will always be called to obtain the actual value, so you can do whatever
guarding there that you need to do.

It tries not to throw errors, but if you give a config that will cause big
problems it will try to throw as soon as the issue is detected, so hopefully
you are dealing with that and fixing the config earlier in development, rather
than after a confusing problem manifests for real users.

All this tough love is intended to reduce the odds that Fragger will get into
a confusing state or unpredictably fail to round trip your data. 


Developing
----------

The tests under test/ use mocha. To run the tests in the checkout, just run
mocha from the checkout. Features should come with tests that they work as
expected. Bug fixes should normally have regression tests.


License
-------

This is MIT licensed (see LICENSE in this repository).
