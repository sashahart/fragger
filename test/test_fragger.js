var assert = require("assert");
var Fragger = require("../fragger").Fragger;

function reverse(text) {
	return text.split("").reverse().join("");
}

describe('fragment_to_object', function() {
	var fn = Fragger._fragment_to_object;

	describe('rejections', function() {
		it('rejects no-args', function() {
			assert.throws(function() {
				fn();
			}, Error);
			assert.throws(function() {
				fn(undefined);
			}, Error);
		});
		it('rejects null', function() {
			assert.throws(function() {
				fn(null);
			}, Error);
		});
		it('rejects empty string', function() {
			assert.throws(function() {
				fn('');
			}, Error);
		});
		it('rejects just hash char', function() {
			assert.throws(function() {
				fn('#');
			}, Error);
		});
	});

	it('empty schema', function() {
		var schema = {};
		var result = fn('a=b', schema);
		assert.equal(result.a, undefined);
	});
	it('simple', function() {
		var schema = {
			a: {
				decode: function(text)  {
					return text;
				}
			},
			b: {
				decode: function(text)  {
					return reverse(text);
				}
			}
		};
		var options = {
			sep: ',',
			eq: '=',
		};
		var result = fn('a=baa,c=daa,b=caa', schema, options);
		assert.equal(result.a, 'baa');
		assert.equal(result.b, 'aac');
		assert.equal(result.c, undefined);
	});
	it('key present but no value parses with identity function', function() {
		var schema = {
			a: null,
		};
		var options = {
			sep: ',',
			eq: '=',
		};
		var result = fn('a=59', schema, options);
		assert.equal(result.a, '59');
	});
	it('key present but empty value parses with identity function', function() {
		var schema = {
			a: {},
		};
		var options = {
			sep: ',',
			eq: '=',
		};
		var result = fn('a=59', schema, options);
		assert.equal(result.a, '59');
	});
	it('implicit value', function() {
		var schema = {
			a: {
				decode: function(text)  {
					return text;
				}
			},
		};
		var options = {
			sep: ',',
			eq: '=',
			implicit_value: 'yams',
		};
		var result = fn('a,b', schema, options);
		assert.equal(result.a, options.implicit_value);
		assert.equal(result.b, undefined);
	});
});

describe('object_to_fragment', function() {
	var fn = Fragger._object_to_fragment;
	var options = {
		// debug: true,
		eq: '=',
		sep: ',',
	};

    it('rejects undefined', function() {
        assert.equal(fn(undefined), null);
    });
    it('rejects null', function() {
        assert.equal(fn(null), undefined);
    });
    it('rejects empty object', function() {
        assert.equal(fn({}), null);
    });
    it('rejects empty schema', function() {
        assert.equal(fn({a: 'abc'}, {}), null);
    });
    it('rejects undefined entry in schema', function() {
        var schema = {a: undefined};
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('rejects null entry in schema', function() {
        var schema = {a: null};
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('rejects empty entry in schema', function() {
        var schema = {
            a: {}
        };
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('handles a simple schema', function() {
        var schema = {
            a: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            }
        };
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, 'a=cba');
    });
    it('handles a simple schema with several values', function() {
        var schema = {
            a: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            },
            b: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            },
            c: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            }
        };
        var data = {
            a: 'abc',
            b: 'def',
            c: 'ghi',
        };
        var result = fn(data, schema, options);
        assert.equal(result, 'a=cba,b=fed,c=ihg');
    });
    it('listens to the validate function', function() {
        var schema = {
            a: {
                encode: reverse,
                validate: function(obj) {
                    return false;
                }
            }
        };
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('rejects schema without encode', function() {
        var schema = {
            a: {
                validate: function(obj) {
                    return true;
                }
            }
        };
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('rejects schema without validate', function() {
        var schema = {
            a: {
                encode: reverse,
            }
        };
        var result = fn({a: 'abc'},  schema, options);
        assert.equal(result, null);
    });
    it('implicit value', function() {
        var schema = {
            a: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            },
            b: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            }
        };
        var local_options = {
            implicit_value: 'zap',
            eq: '=',
            sep: ',',
        };
        var result;
        result = fn({a: 'zap'},  schema, local_options);
        assert.equal(result, 'a');
        result = fn({a: 'bam', b: 'baz'}, schema, local_options);
        assert.equal(result, 'a=mab,b=zab');
        result = fn({a: 'bam', b: 'zap'}, schema, local_options);
        assert.equal(result, 'a=mab,b');
    });
    it('omitted values', function() {
        var schema = {
            a: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            },
            b: {
                encode: reverse,
                validate: function(obj) {
                    return true;
                }
            }
        };
        result = fn({a: undefined, b: 'yep'}, schema, options);
        assert.equal(result, 'b=pey');
        result = fn({a: null, b: 'yep'}, schema, options);
        assert.equal(result, 'b=pey');
        result = fn({a: false, b: 'yep'}, schema, options);
        assert.equal(result, 'b=pey');
        result = fn({a: 'oof', b: 'yep'}, schema, options);
        assert.equal(result, 'a=foo,b=pey');
    });
});

describe('Fragger', function(){
	var cls = Fragger;
    var good_schema = {
        a: {
            validate: function() {
                return true;
            },
            encode: function(obj) {
                return obj;
            },
            // decode effectively defaults to function(x) { return x }
        }
    };

    it('throws an error for no schema', function() {
        assert.throws(function() { cls(undefined);}, Error);
        assert.throws(function() { cls(null);}, Error);
    });
    it('throws an error for empty schema', function() {
        assert.throws(function() { cls({});}, Error);
    });
    it('throws an error for undefined schema entry', function() {
        assert.throws(function() { 
            cls({
                a: undefined
            });
        }, Error);
    });
    it('throws an error for null schema entry', function() {
        assert.throws(function() { 
            cls({
                a: null
            });
        }, Error);
    });
    it('throws an error for empty schema entry', function() {
        assert.throws(function() { 
            cls({
                a: {}
            });
        }, Error);
    });
    it('throws an error for schema entry missing validate', function() {
        assert.throws(function() { 
            cls({
                a: {
                    encode: function(x) {}, 
                }
            });
        }, Error);
    });
    it('throws an error for schema entry missing encode', function() {
        assert.throws(function() { 
            cls({
                a: {
                    validate: function(x) {}, 
                }
            });
        }, Error);
    });
    it('throws an error for schema entry missing encode', function() {
    });
    it('throws an error for bad schema entry', function() {
        assert.throws(function() { 
            cls({
                a: {
                    validate: null, 
                    encode: null, 
                }
            });
        }, Error);
        assert.doesNotThrow(function() {
            cls({
                a: {
                    validate: function(x) {}, 
                    encode: function(x) {} 
                }
            });
        });
    });
    it('throws an error for empty sep', function() {
        assert.throws(function() { cls(good_schema, {sep: ''});}, Error);
    });
    it('throws an error for empty eq', function() {
        assert.throws(function() { cls(good_schema, {eq: ''});}, Error);
    });
    it('throws an error for invalid max_length', function() {
        assert.throws(function() { cls(good_schema, {max_length: 0});}, Error);
        assert.throws(function() { cls(good_schema, {max_length: -1});}, Error);
    });
    it('round trips with encode and decode', function() {
        var schema = {
            a: good_schema.a,
            c: good_schema.a,
        };
        var fragger = cls(schema);
        var original = {a: 'b', c: 'def'};  
        var encoded = fragger.encode(original);
        var decoded = fragger.decode(encoded);
        var property;
        for (property in decoded) {
            if (decoded.hasOwnProperty(property)) {
                assert.equal(decoded[property], original[property]);
            }
        }
        for (property in original) {
            if (original.hasOwnProperty(property)) {
                assert.equal(decoded[property], original[property]);
            }
        }
    });
});
